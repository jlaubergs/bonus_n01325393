﻿<%@ Page Language="C#" CodeBehind="Cartesian.aspx.cs" Inherits="bonus_n01325393.Cartesian" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>Cartesian Smartesian</title>
</head>
<body>
    <a href="/">Back</a>
	<form id="form1" runat="server">
        <h1>Cartesian Smartesian</h1>
        <div>
            <label for="xValue">X Value <span style="color:red;">*</span></label>
            <asp:TextBox runat="server" ID="xValue" placeholder="e.g. 3"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" id="xValueRequiredValid" controltovalidate="xValue" errormessage="Please enter X value" />
            <asp:RegularExpressionValidator ID="xValueRegExValid" runat="server" ValidationExpression="^-?[0-9]\d*$" ControlToValidate="xValue" ErrorMessage="Enter a valid full number"></asp:RegularExpressionValidator>
        </div>  
        <div>
            <label for="yValue">Y Value <span style="color:red;">*</span></label>
            <asp:TextBox runat="server" ID="yValue" placeholder="e.g. -4"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" id="yValueRequiredValid" controltovalidate="yValue" errormessage="Please enter Y value" />
            <asp:RegularExpressionValidator ID="yValueRegExValid" runat="server" ValidationExpression="^-?[0-9]\d*$" ControlToValidate="yValue" ErrorMessage="Enter a valid full number"></asp:RegularExpressionValidator>
                
        </div>  
            
        <asp:Button ID="cartesianSubmit" OnClick="CartesianSubmit" Text="Submit" runat="server" />
        
        <div runat="server" ID="cartesianResults"></div>
	</form>
</body>
</html>
