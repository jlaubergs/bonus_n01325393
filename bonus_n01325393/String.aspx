﻿<%@ Page Language="C#" CodeBehind="String.aspx.cs" Inherits="bonus_n01325393.String" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>String Bling</title>  
</head>
<body>
    <a href="/">Back</a>
	<form id="form1" runat="server">
        <h1>String Bling</h1>
        <div>
            <label for="wordInput">Word <span style="color:red;">*</span></label>
            <asp:TextBox runat="server" ID="wordInput" placeholder="e.g. Car"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" id="wordInputRequiredValid" controltovalidate="wordInput" errormessage="Please enter a word" />
            <asp:RegularExpressionValidator ID="wordInputRegExValid" runat="server" ValidationExpression="^[a-zA-Z_ ]*$" ControlToValidate="wordInput" ErrorMessage="Enter a valid word"></asp:RegularExpressionValidator>
        </div>   
            
        <asp:Button ID="stringSubmit" OnClick="StringSubmit" Text="Submit" runat="server" />
        
        <div runat="server" ID="stringResults"></div>
	</form>
</body>
</html>
