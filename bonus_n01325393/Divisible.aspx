﻿<%@ Page Language="C#" CodeBehind="Divisible.aspx.cs" Inherits="bonus_n01325393.Divisible" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>Divisible Sizzable</title>
</head>
<body>
    <a href="/">Back</a>
	<form id="form1" runat="server">
        <h1>Divisible Sizzable</h1>
        <div>
            <label for="numberValue">Number <span style="color:red;">*</span></label>
            <asp:TextBox runat="server" ID="numberValue" placeholder="e.g. 3"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" id="numberValueRequiredValid" controltovalidate="numberValue" errormessage="Please enter a number" />
            <asp:RegularExpressionValidator ID="numberValueRegExValid" runat="server" ValidationExpression="^-?[0-9]\d*$" ControlToValidate="numberValue" ErrorMessage="Enter a valid full number"></asp:RegularExpressionValidator>
        </div>   
            
        <asp:Button ID="divisibleSubmit" OnClick="DivisibleSubmit" Text="Submit" runat="server" />
        
        <div runat="server" ID="divisibleResults"></div>
	</form>
</body>
</html>
