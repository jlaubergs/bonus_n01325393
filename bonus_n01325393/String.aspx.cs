﻿using System;
using System.Web;
using System.Web.UI;

namespace bonus_n01325393
{
    public partial class String : System.Web.UI.Page
    {
        public void StringSubmit(object sender, EventArgs args)
        {

            string word = wordInput.Text.ToString();
            string reversedWord = "";
            string res = "";

            word = word.ToLower().Replace(" ", string.Empty);

            for (int i = word.Length-1; i >= 0; i--){
                reversedWord += word[i];
            }

            if( reversedWord == word ){
                res = "The word is a palindrome";
            } else {
                res = "The word is not a palindrome";
            }

            stringResults.InnerHtml = res;

        }
    }
}
