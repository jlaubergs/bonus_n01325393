﻿using System;
using System.Web;
using System.Web.UI;

namespace bonus_n01325393
{

    public partial class Cartesian : System.Web.UI.Page
    {
        public void CartesianSubmit(object sender, EventArgs args){

            int x_value = int.Parse(xValue.Text);
            int y_value = int.Parse(yValue.Text);
            string res = "0";

            if( x_value > 0 ){
                if( y_value > 0 ){
                    res = "1st";
                } else if ( y_value < 0 ){
                    res = "4th";
                }
            } else if ( x_value < 0 ){
                if (y_value > 0)
                {
                    res = "2nd";
                }
                else if (y_value < 0)
                {
                    res = "3rd";
                }
            }

            if( res != "0" ){
                cartesianResults.InnerHtml = "You are in the " + res + " quadrant!";
            } else {
                cartesianResults.InnerHtml = "You are in between quadrants!";
            }




        }
    }
}
