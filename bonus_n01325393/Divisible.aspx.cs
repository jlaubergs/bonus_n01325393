﻿using System;
using System.Web;
using System.Web.UI;

namespace bonus_n01325393
{
    public partial class Divisible : System.Web.UI.Page
    {
        public void DivisibleSubmit(object sender, EventArgs args)
        {

            int number = int.Parse(numberValue.Text);
            string res = "";
            if( number > 1 ){
                res = "Is a prime number";
            } else {
                res = "Not a prime number";
            }

            for (int i = number - 1; i > 1; i-- ){
                if( number % i == 0 ){
                    res = "Not a prime number";
                }
            }

            divisibleResults.InnerHtml = res;

        }
    }
}
